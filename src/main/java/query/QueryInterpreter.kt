package query

import model.Expression

interface QueryInterpreter {

    fun interpret(query: String): Expression

}