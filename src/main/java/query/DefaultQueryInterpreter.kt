package query

import model.*

class DefaultQueryInterpreter : QueryInterpreter {

    private object Operations {
        const val AND = "&"
        const val NOT_AND = "!&"
        const val OR = "|"
        const val NOT_OR = "!|"
    }

    private interface ParseState {
        fun parse(i: Int, str: String): Expression
    }

    private inner class LeftExpState : ParseState {

        override fun parse(i: Int, str: String): Expression {
            val value = StringBuilder()

            for (j in i until str.length) {
                val ch = str[j]

                if (isWord(ch)) {
                    value.append(str[j])
                } else {
                    throwIfEmpty(value.toString(), { "Blank value for a left expression at $j, expression ${str.substring(i)} for query $str" })
                    return OperExpState(Value(value.toString())).parse(dropSpaces(j, str), str)
                }
            }
            throwIfEmpty(value.toString(), { "Blank expression" })
            // EOS
            return Exact.of(Value(value.toString()))
        }
    }

    private inner class OperExpState(private val left: Value) : ParseState {

        override fun parse(i: Int, str: String): Expression {
            val oper = StringBuilder()

            for (j in i until str.length) {
                val ch = str[j]

                if (isOper(ch)) {
                    oper.append(ch)
                } else {
                    throwIfEmpty(oper.toString(), { "Blank value for an operation expression at $j, expression ${str.substring(i)} for query $str" })
                    return RightExpParser(left, operation(oper.toString())).parse(dropSpaces(j, str), str)
                }
            }
            throw IllegalStateException("Couldn't parse operation in expression starting from index $i, expression ${str.substring(i)}")
        }

        private fun operation(oper: String): (Value, Value) -> Expression = when (oper) {
            Operations.AND -> { left, right -> And.and(left, right) }
            Operations.NOT_AND -> { left, right -> And.and(left, Not.not(Exact.of(right))) }
            Operations.OR -> { left, right -> Or.or(left, right) }
            Operations.NOT_OR -> { left, right -> Or.or(left, Not.not(Exact.of(right))) }
            else -> throw Exception("Illegal operation $oper")
        }

    }

    private inner class StatefulOperExpParser(private val exp: Expression) : ParseState {

        override fun parse(i: Int, str: String): Expression {
            val oper = StringBuilder()

            for (j in i until str.length) {
                val ch = str[j]

                if (isOper(ch)) {
                    oper.append(ch)
                } else {
                    throwIfEmpty(oper.toString(), { "Blank value for an operation expression at $j, expression ${str.substring(i)} for query $str" })
                    return RightStatefulExpParser(exp, operation(oper.toString())).parse(dropSpaces(j, str), str)
                }
            }
            throw IllegalStateException("Couldn't parse operation in expression starting from index $i, expression ${str.substring(i)}")
        }

        private fun operation(oper: String): (Value, Expression) -> Expression = when (oper) {
            Operations.AND -> { left, right -> And.and(left, right) }
            Operations.NOT_AND -> { left, right -> And.and(left, Not.not(right)) }
            Operations.OR -> { left, right -> Or.or(left, right) }
            Operations.NOT_OR -> { left, right -> Or.or(left, Not.not(right)) }
            else -> throw Exception("Illegal operation $oper")
        }

    }

    private inner class RightStatefulExpParser(private val exp: Expression, private val factory: (Value, Expression) -> Expression) : ParseState {
        override fun parse(i: Int, str: String): Expression {
            val value = StringBuilder()

            for (j in i until str.length) {
                val ch = str[j]

                if (isWord(ch)) {
                    value.append(ch)
                } else {
                    throwIfEmpty(value.toString(), { "Blank value for a right expression at $j, expression ${str.substring(i)} for query $str" })
                    return FinalExpParser(factory(Value(value.toString()), exp)).parse(dropSpaces(j, str), str)
                }
            }
            throwIfEmpty(value.toString(), { "Blank right value of expression" })
            // EOS
            return FinalExpParser(factory(Value(value.toString()), exp)).parse(dropSpaces(str.length - 1, str), str)
        }

    }

    private inner class RightExpParser(private val left: Value, private val factory: (Value, Value) -> Expression) : ParseState {
        override fun parse(i: Int, str: String): Expression {
            val value = StringBuilder()

            for (j in i until str.length) {
                val ch = str[j]

                if (isWord(ch)) {
                    value.append(ch)
                } else {
                    throwIfEmpty(value.toString(), { "Blank value for a right expression at $j, expression ${str.substring(i)} for query $str" })

                    val k = dropSpaces(j, str)

                    return FinalExpParser(factory(left, Value(value.toString()))).parse(k, str)
                }
            }
            throwIfEmpty(value.toString(), { "Blank right value of expression" })
            // EOS
            return FinalExpParser(factory(left, Value(value.toString()))).parse(str.length - 1, str)
        }

    }

    private inner class FinalExpParser(private val exp: Expression) : ParseState {

        override fun parse(i: Int, str: String) = if (i == str.length - 1) exp else StatefulOperExpParser(exp).parse(i, str)

    }

    override fun interpret(query: String): Expression {
        return LeftExpState().parse(0, query.trim())
    }

    private fun isOper(ch: Char) = "&|!".indexOf(ch) >= 0

    private fun isWord(ch: Char) = Character.isLetterOrDigit(ch) && !isOper(ch)

    private fun isSpace(ch: Char) = Character.isSpaceChar(ch)

    private fun dropSpaces(i: Int, str: String) = (i until str.length).firstOrNull { !isSpace(str[it]) } ?: str.length-1

    private fun throwIfEmpty(str: String, lazyMessage: () -> Any) {
        if (str.isEmpty()) {
            throw IllegalArgumentException(lazyMessage().toString())
        }
    }

}