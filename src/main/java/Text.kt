import java.util.regex.Pattern

val SPLIT_PATTERN = Pattern.compile("[ \t\n\r\\p{Punct}]+")