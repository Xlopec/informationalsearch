/*
 * JBookReader - Java FictionBook Reader
 * Copyright (C) 2006 Dmitry Baryshkov
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *   
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package fb2.parser;


import fb2.*;
import fb2.imp.Book;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.File;
import java.io.IOException;

public class FB2Parser {

	public static IBook parse(String uri) throws IOException, SAXException {
		return parse(new InputSource(uri));
	}

	private static IBook parse(InputSource source) throws IOException, SAXException {
		XMLReader reader;
		
		reader = XMLReaderFactory.createXMLReader();
		reader.setErrorHandler(new ParseErrorHandler());

		IBook book = new Book();

		reader.setContentHandler(new FB2ContentsHandler(book));

		reader.parse(source);

		return book;	
	}

	public static IBook parse(File file) throws IOException, SAXException {
		return parse(file.getAbsolutePath());
	}

	private static class FB2ContentsHandler extends DefaultHandler {

		@SuppressWarnings("unused")
		private Locator myLocator;

		private final IBook myBook;

		private IContainerNode myContainer;

		private StringBuilder myText = new StringBuilder();

		private boolean hadOpenTag = false;

		private boolean hadCloseTag;

		private boolean parseXML = false;

		private boolean myParseText = false;

		public FB2ContentsHandler(IBook book) {
			this.myBook = book;
		}

		@Override
		public void setDocumentLocator(Locator locator) {
			this.myLocator = locator;
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			if (!this.parseXML) {
				return;
			}
			
			if (!this.myParseText) {
				return;
			}
			
			this.myText.append(ch, start, length);
		}

		private void processTextNode() {
			if (!this.myParseText) {
				return;
			}
			
			String string = trimStringBuilder(this.myText, this.hadOpenTag, this.hadCloseTag);
//			String string = this.myText.toString();
			this.myText.setLength(0);
			
			if (string.length() == 0) {
				return;
			}

			this.myContainer.newTextNode(string);

//			System.out.println("#text: '" + string + "'");
		}

		private boolean isParagraphTag(String tagName) {
			return tagName.equals("p")
					|| tagName.equals("subtitle")
					|| tagName.equals("text-author")
					|| tagName.equals("v");

		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			if (!this.parseXML) {
				return;
			}

			if (localName.equals("FictionBook")) {
				// XXX: root node;
			} else if (localName.equals("binary")) {
				this.myText.setLength(0);
				this.myParseText = false;
			} else {
				// part of body
				this.hadCloseTag = true;
				processTextNode();
				this.hadOpenTag = false;
				
				if (isParagraphTag(localName)) {
					this.myParseText = false;
				}
				
				if (this.myContainer.getTagName().equals(localName)) {
					this.myContainer = this.myContainer.getParentNode();
				}

			}
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) {
			if (!this.parseXML) {
				if (!localName.equals("body")) {
					return;
				}
				
				this.parseXML = true;
			}

			if (localName.equals("FictionBook")) {
				// XXX: root book node
			} else {
				INode node = null;

				this.hadCloseTag = false;
				processTextNode();
				this.hadOpenTag = true;
			
				if (localName.equals("body")) {
					node = this.myBook.newBody("body", attributes.getValue("name"));
				} else if (localName.equals("title")) {
					node = this.myContainer.newTitle(localName);
				} else if (localName.equals("image")) {
					return;
				} else if (isParagraphTag(localName)) {
					node = this.myContainer.newContainerNode(localName);
	
					this.myParseText  = true;
				} else {
					/*
					 * Threat every unknown node as a simple container.
					 */
					node = this.myContainer.newContainerNode(localName);
				}
				
				String classAttribute;
				
				if (localName.equals("p")) {
					classAttribute = "style";
				} else if (localName.equals("style")) {
					classAttribute = "name";
				} else {
					classAttribute = "class";
				}
				node.setNodeClass(attributes.getValue(classAttribute));
				
				if (node instanceof IContainerNode) {
					this.myContainer = (IContainerNode)node;
				}

				{
					String id;
					if ((id = attributes.getValue("id")) != null) {
						node.setID(id);
					}
				}
			}



		}

		private static String trimStringBuilder(StringBuilder builder, boolean trimStart, boolean trimEnd) {
			int length = builder.length();
			if ((length == 0)
					|| (builder.charAt(0) > '\u0020')
					&& (builder.charAt(length-1) > '\u0020')) {
				return builder.toString();
			}

			int begin = 0;
			int end = length-1;
			if (trimStart) {
				while ((begin <= end) && (builder.charAt(begin) <= '\u0020')) {
					begin++;
				}
			}
			if (trimEnd) {
				while ((begin <= end) && (builder.charAt(end) <= '\u0020')) {
					end--;
				}
			}
			if (begin > end) {
				return "";
			}
			return builder.substring(begin, end+1);
		}
		
	}

}
