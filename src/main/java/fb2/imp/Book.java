/*
 * JBookReader - Java FictionBook Reader
 * Copyright (C) 2006 Dmitry Baryshkov
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *   
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package fb2.imp;


import fb2.IBook;
import fb2.IContainerNode;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Book implements IBook {

	private Map<String, IContainerNode> myBodies = new LinkedHashMap<String, IContainerNode>();

	public IContainerNode newBody(String tagName, String name) {
		ContainerNode body = new ContainerNode();
		body.setTagName(tagName);
		body.setBook(this);
		this.myBodies.put(name, body);
		return body;
	}

	public IContainerNode getMainBody() {
		IContainerNode node = this.myBodies.get(null);
		if (node == null) {
			throw new IllegalStateException("No main body provided");
		}
		return node;
	}
	
	public Collection<IContainerNode> getBodies() {
		return Collections.unmodifiableCollection(this.myBodies.values());
	}

}
