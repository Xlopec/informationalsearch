/*
 * JBookReader - Java FictionBook Reader
 * Copyright (C) 2006 Dmitry Baryshkov
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *   
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package fb2.imp;


import fb2.IContainerNode;
import fb2.INode;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

final class ContainerNode extends AbstractNode implements IContainerNode {

	private List<INode> myChildNodes = new LinkedList<INode>();

	private IContainerNode myTitle;

	public List<INode> getChildNodes() {
		return Collections.unmodifiableList(this.myChildNodes);
	}
	

	protected void addChildNode(AbstractNode node) {
		this.myChildNodes.add(node);
		node.setBook(this.getBook());
		node.setParentNode(this);
	}
	
	public INode newTextNode(String text) {
		TextNode node = new TextNode();
		node.setText(text);
		this.addChildNode(node);
		return node;
	}

	public IContainerNode newContainerNode(String tagName) {
		ContainerNode node = new ContainerNode();
		node.setTagName(tagName);
		this.addChildNode(node);
		return node;
	}

	@Override
	public void setTagName(String tagName) {
		super.setTagName(tagName);
	}

	public IContainerNode getTitle() {
		return this.myTitle;
	}

	public IContainerNode newTitle(String tagName) {
		ContainerNode node = new ContainerNode();
		node.setTagName(tagName);
		this.addChildNode(node);
		this.myTitle = node;
		return node;
	}

}
