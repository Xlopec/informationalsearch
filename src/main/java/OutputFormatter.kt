import model.Dictionary

interface StatsFormatter {

    fun printStats(dictionary: Dictionary)

}