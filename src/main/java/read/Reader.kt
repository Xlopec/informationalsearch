package read

import model.Book
import java.io.InputStream

interface Reader {

    fun read(name: String, inputStream: InputStream) : Book

}