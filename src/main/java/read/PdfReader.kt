package read

import model.Book
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream
import org.apache.pdfbox.pdfparser.PDFParser
import org.apache.pdfbox.text.PDFTextStripper
import java.io.InputStream
import java.util.regex.Pattern

class PdfReader : Reader {

    object Patterns {
        val PATTERN: Pattern = Pattern.compile("[ \n\r\\p{Punct}]")
    }

    private val stripper = PDFTextStripper()

    override fun read(name: String, inputStream: InputStream): Book {
        val parser = PDFParser(RandomAccessBufferedFileInputStream(inputStream))

        parser.parse()

        try {
            return Book(name, toContent(stripper.getText(parser.pdDocument)))
        } finally {
            parser.pdDocument.close()
        }
    }

    private fun toContent(s: String) = s.split(Patterns.PATTERN).filter { w -> w.isNotEmpty() }

}