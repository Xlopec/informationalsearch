import model.Dictionary

class DefaultOutputFormatter : StatsFormatter {

    override fun printStats(dictionary: Dictionary) {

        println("Words in dictionary ${dictionary.countWords()}")
    }
}