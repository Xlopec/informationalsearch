package model

class Dictionary {

    private val _words = HashSet<String>()

    val words = _words

    fun addText(text: Text) {
        _words.addAll(text.words)
    }

    fun removeText(text: Text) {
        _words.removeAll(text.words)
    }

    fun countWords() = _words.size

}