package model

import java.util.*
import kotlin.collections.HashMap

class InvertedDistanceIndex(private val books: Array<out Book>) : SearchIndex {

    private data class Entry(private val books: List<String>, private var j: Int) {
        fun next() {
            j += 1
        }

        fun hasNext() = j < books.lastIndex
        fun isEmpty() = books.isEmpty()
        fun book() = books[j]
    }

    private val entries: Map<String, SortedSet<String>>

    init {
        entries = books.fold(HashMap(), { entries, book ->
            // word to book
            book.text.words
                    .map { Pair(it, book.name) }
                    .groupBy({ it.first }, { it.second })
                    .forEach { pair ->
                        var entry = entries[pair.key]

                        if (entry == null) {
                            entry = TreeSet()
                            entries[pair.key] = entry
                        }

                        entry.addAll(pair.value)
                    }
            entries
        })
    }

    override fun search(exp: Expression): Set<Book> {
        if (books.isEmpty()) {
            return emptySet()
        }

        val terms = exp.terms()
        val resultBooks = HashSet<Book>()
        val vectors = HashMap<String, Entry>(terms.size)

        terms.forEach { vectors[it.value] = Entry(entries[it.value]?.toList() ?: emptyList(), 0) }

        val vector = vectors.values.toList()
        val findWord = { value: Value -> vectors[value.value] != null }

        val satisfiesExp = exp.eval(findWord)

        do {
            if (satisfiesExp) {
                books.find { !vector[0].isEmpty() && it.name == vector[0].book() }?.apply {
                    resultBooks += this
                }
            }
        } while (satisfiesExp && nextBook(vector))

        return resultBooks
    }

    private fun nextBook(vector: List<Entry>): Boolean {
        var hasNext = false

        fun next(l: Int, r: Int) {
            while (!vector[l].isEmpty() && !vector[r].isEmpty()
                    && vector[l].book() < vector[r].book() && vector[l].hasNext()) {
                vector[l].next()
            }
        }

        for (i in vector.indices - 1) {

            hasNext = hasNext || vector[i].hasNext() || vector[i + 1].hasNext()

            next(i, i + 1)

            next(i + 1, i)

            // book[j] == book[j+1]
            if ((vector[i].hasNext() || vector[i + 1].hasNext()) && vector[i].book() != vector[i + 1].book()) {
                throw IllegalStateException("Shit for entry ${vector[i]} and ${vector[i + 1]}")
            }

            if (vector[i].hasNext()) {
                vector[i].next()
            }

            if (vector[i + 1].hasNext()) {
                vector[i + 1].next()
            }
        }

        return hasNext
    }

}