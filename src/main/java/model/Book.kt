package model

class Book(val name: String, words: List<String>) {
    val text = Text(words)

    fun wordsCount() = text.words.size

    override fun toString(): String {
        return "$name ${text.words}"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Book

        if (name != other.name) return false
        if (text != other.text) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + text.hashCode()
        return result
    }
}

class Text(val words: Collection<String>)