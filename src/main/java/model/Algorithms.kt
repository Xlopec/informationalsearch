package model

import java.util.ArrayList

internal fun kGram(arg: String, k: Int = 3): List<String> {
    fun kGram(w: String): List<String> {
        val grams = ArrayList<String>(w.length)
        var i = 0

        while (i <= w.length - k) {
            grams += w.substring(i, i + k)
            i++
        }

        return grams
    }

    val list = ArrayList<String>(arg.length)

    for (w in "$$arg$".split('*')) {
        list += kGram(w)
    }

    list.trimToSize()
    return list
}

internal fun shuffleIndex(arg: String): List<String> {
    var word = arg + "$"
    val list = ArrayList<String>(word.length)

    list.add(word)

    var i = word.lastIndex

    while (i > 0) {
        word = word.substring(1, i) + word.substring(i) + word[0]
        list += word
        i--
    }

    return list
}

internal fun shuffleJoker(arg: String): String {
    if (!arg.contains('*')) {
        throw IllegalArgumentException()
    }

    var word = arg + "$"
    var i = word.lastIndex

    while (i > 0 && word[word.lastIndex] != '*') {
        word = word.substring(1, i) + word.substring(i) + word[0]
        i--
    }

    return word
}