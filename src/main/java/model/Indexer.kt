package model

import java.io.Closeable
import java.io.File

interface Indexer {

    fun createIndex(dir: File, files: Collection<File>) : File

}

interface IndexStorage {

    data class Term(val token: String, val docs: Collection<String>)

    interface WriteOperation : Closeable {
        fun onWrite(term: Term)
    }

    fun writeOperation() : WriteOperation

}