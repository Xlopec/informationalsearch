package model

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import java.io.BufferedReader
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader
import java.util.*

class SPIMI(private val blockSize: Int = 10, private val verbose: Boolean = false,
            private val storage: IndexStorage, private val tokenize: suspend (File) -> Iterable<String>) : Indexer {

    init {
        require(blockSize > 0)
    }

    data class Term(val token: String, val docs: Iterable<String>)

    private class Block(file: File) {
        private val fis = BufferedReader(FileReader(file))
        var current = nextTerm()

        init {
            closeIfNeeded()
        }

        fun next(): Term? {
            val t = current

            current = nextTerm()
            closeIfNeeded()
            return t
        }

        fun isMerged() = current == null

        private fun nextTerm(): Term? = fis.readLine()?.let {
            Term(token = it.substring(0, it.indexOf(':')),
                    docs = it.substring(it.indexOf(':') + 1).split('"').filter { it.isNotEmpty() })
        }

        private fun closeIfNeeded() {
            if (isMerged()) {
                fis.close()
            }
        }
    }

    override fun createIndex(dir: File, files: Collection<File>): File {
        require(dir.isDirectory && dir.canRead() && dir.canWrite())

        val result = File(dir, "result")

        debug {
            "Building index for files ${files.size}, out file will be $result"
        }

        val mergeFiles = files.chunked(blockSize)
                .mapIndexed { index, chunked -> Pair(File(dir, "part_$index"), chunked) }
                .map { (file, chunk) -> async { file.createNewFile(); writeBlock(file, chunk); file } }

        merge(runBlocking { mergeFiles.map { it.await() } }, result)
        debug { "Index was built successfully, result file is $result" }
        return result
    }

    private suspend fun writeBlock(out: File, files: List<File>) {
        debug { "Writing block $out" }

        val tokenToIds = TreeMap<String, MutableSet<String>>()

        files.forEach {
            debug { "Tokenizing $it" }
            tokenize(it).fold(tokenToIds) { acc, t -> acc.getOrPut(t) { HashSet() }.add(it.name);acc }
        }

        FileOutputStream(out).use {
            tokenToIds.forEach { token, docs -> it.write(formatTerm(token, docs).toByteArray()) }
        }

        debug { "Written block $out" }
    }

    private fun merge(files: Collection<File>, out: File) {
        debug { "Merging ${files.size} files into $out" }

        if (out.delete()) {
            out.createNewFile()
        }

        storage.writeOperation().use {
            merge(files) { term, docs ->
                it.onWrite(IndexStorage.Term(term, docs))
            }
        }
    }

    private fun merge(files: Collection<File>, write: (String, Collection<String>) -> Unit) {
        val mergeBlocks = files.map { Block(it) }

        fun minTerm() = mergeBlocks
                .mapNotNull { it.current }
                .minWith(kotlin.Comparator({ o1, o2 -> o1.token.compareTo(o2.token) }))

        var min = minTerm()

        while (min != null) {
            val processingBlocks = mergeBlocks.filter { it.current?.token == min!!.token }

            val merged = processingBlocks
                    .map { it.current!!.docs }
                    .flatten()
                    .sorted()

            write(min.token, merged)
            processingBlocks.forEach { it.next() }

            debug { "Written info for token ${min!!.token}" }

            min = minTerm()
        }

        require(mergeBlocks.all { it.isMerged() }) {
            "Block wasn't merged"
        }
    }

    private fun formatTerm(term: String, docs: Iterable<String>) = "$term:${docs.joinToString(separator = "") { "\"$it\"" }}\n"

    private fun debug(message: () -> Any) {
        if (verbose) {
            println(message())
        }
    }

}