package model

import java.util.*
import kotlin.collections.HashMap

class InvertedKGramIndex(private val books: Array<out Book>, private val k: Int = 3) : SearchIndex {

    private data class Entry(private val books: List<String>) {
        private var j = 0

        fun next() {
            j += 1
        }

        fun hasNext() = j < books.lastIndex
        fun isEmpty() = books.isEmpty()
        fun book() = books[j]
    }

    private val index: Map<String, SortedSet<String>>

    init {
        index = books.fold(HashMap(), { entries, book ->
            // word to book
            book.text.words
                    .map { kGram(it, k) }
                    .flatten()
                    .map { Pair(it, book.name) }
                    .groupBy({ it.first.toLowerCase() }, { it.second })
                    .forEach { pair ->
                        var entry = entries[pair.key]

                        if (entry == null) {
                            entry = TreeSet()
                            entries[pair.key] = entry
                        }

                        entry.addAll(pair.value)
                    }
            entries
        })
    }

    override fun search(exp: Expression): Set<Book> {
        if (books.isEmpty()) {
            return emptySet()
        }

        val vectors = vectors(exp.terms())
        val vector = vectors.values.toList()

        if (!exp.eval({ vectors[it.value] != null })) {
            return emptySet()
        }

        val resultBooks = HashSet<Book>()

        do {
            books.find { !vector[0].isEmpty() && it.name == vector[0].book() }?.apply {
                resultBooks += this
            }
        } while (nextBook(vector))

        return resultBooks
    }

    private fun vectors(terms: Set<Value>) = terms.fold(HashMap<String, Entry>(terms.size), { map, v ->
        map[v.value] = Entry(index[v.value]?.toList() ?: emptyList()); map
    })

    private fun nextBook(vector: List<Entry>): Boolean {
        var hasNext = false

        fun next(l: Int, r: Int) {
            while (!vector[l].isEmpty() && !vector[r].isEmpty()
                    && vector[l].book() < vector[r].book() && vector[l].hasNext()) {
                vector[l].next()
            }
        }

        for (i in 0..vector.size - 2) {

            hasNext = hasNext || vector[i].hasNext() || vector[i + 1].hasNext()

            next(i, i + 1)
            next(i + 1, i)
            // book[j] == book[j+1]
            if (vector[i].hasNext() && vector[i + 1].hasNext() && vector[i].book() != vector[i + 1].book()) {
                throw IllegalStateException("Invalid condition for entry ${vector[i]} and ${vector[i + 1]}")
            }

            if (vector[i].hasNext()) {
                vector[i].next()
            }

            if (vector[i + 1].hasNext()) {
                vector[i + 1].next()
            }
        }

        return hasNext
    }

}