package model

import java.util.*
import kotlin.collections.HashMap
import kotlin.math.log10

enum class Zone {
    BODY, TITLE // add whatever you need
}

/**
 * Inverted index that supports:
 * * fuzzy search
 * * tf-idf
 * * zones
 */
class InvertedIndexImp(private val books: Collection<Book>, private val k: Int = 3) : SearchIndex {

    constructor(books: Array<Book>, k: Int = 3) : this(books.toList(), k)

    private data class ZonedEntry(var zones: EnumSet<Zone>, val bookName: String, var tf: Int = 0) : Comparable<ZonedEntry> {
        override fun compareTo(other: ZonedEntry): Int = bookName.compareTo(other.bookName)
        // ignore zone
        override fun equals(other: Any?): Boolean = other != null && other is ZonedEntry && other.bookName == bookName

        override fun hashCode() = 31 * bookName.hashCode()
    }

    private val index: Map<String, TreeSet<ZonedEntry>>

    init {

        val tokenToTf = HashMap<String, Int>(Math.min(books.map { it.text.words.size }.count() / 2, 1))

        index = books.fold(HashMap(), { entries, book ->
            val allTokens = book.text.words
                    .map { it.toLowerCase() }
                    .map { kGram(it, k) }
                    .flatten()

            fun insert(key: String, zone: Zone) {
                val postingSet = entries.getOrPut(key) { TreeSet() }
                val entry = postingSet.find { it.bookName == book.name } ?: ZonedEntry(EnumSet.of(zone), book.name)
                val tf = (tokenToTf[key] ?: 0) + 1

                entry.zones.add(zone)
                entry.tf = tf

                postingSet.add(entry)
                tokenToTf[key] = tf
            }

            // word to book
            allTokens
                    .map { Pair(it, book.name) }
                    .groupBy({ it.first }, { it.second })
                    .forEach { pair ->
                        insert(pair.key, Zone.BODY)
                    }

            val bookTokens = kGram(book.name, k).map { it.toLowerCase() }

            bookTokens.forEach { word ->
                // term posting set
                insert(word, Zone.TITLE)
            }

            entries
        })
    }

    override fun search(exp: Expression): Set<Book> {
        if (index.isEmpty()) {
            return emptySet()
        }

        val gBody = 0.3
        val gTitle = 0.7

        val terms = exp.terms()
        val zoneToQueryToken = terms.groupBy { it.zone }

        val scores = HashMap<String, Double>()

        zoneToQueryToken[Zone.TITLE]?.map { it.value }?.forEach { qW ->
            val index = index[qW]?.filter { it.zones.contains(Zone.TITLE) }

            if (index != null) {
                val qTfIdf = tfIdf(terms.filter { it.value == qW }.size, terms.size, 1)

                index.forEach {
                    var score = scores.getOrPut(qW) { 0.0 }

                    score += qTfIdf * tfIdf(it.tf, books.size, index.size)
                    scores[qW] = score
                }
            }
        }

        zoneToQueryToken[Zone.BODY]?.map { it.value }?.forEach { qW ->
            val index = index[qW]?.filter { it.zones.contains(Zone.BODY) }

            if (index != null) {
                val qTfIdf = tfIdf(terms.filter { it.value == qW }.size, terms.size, 1)

                index.forEach {
                    var score = scores.getOrPut(it.bookName) { 0.0 }

                    score += qTfIdf * tfIdf(it.tf, books.size, index.size)
                    scores[it.bookName] = score
                }
            }
        }


        for ((k, v) in scores) {
            scores[k] = v / books.find { it.name == k }!!.wordsCount()
        }

        val books = scores.entries.toList().sortedWith(Comparator { o1, o2 ->
            o2.value.compareTo(o1.value)
        }).mapNotNull { e -> books.find { it.name == e.key } }.toSet()


        return books
    }

    private fun tfIdf(tf: Int, total: Int, postingListLen: Int): Double {
        var t = 0.0

        if (tf != 0) {
            t = 1 + log10(tf.toDouble())
        }

        return t * log10(total / postingListLen.toDouble())
    }

}