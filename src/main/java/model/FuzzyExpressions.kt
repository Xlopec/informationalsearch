package model

object AndFuzzy {

    fun and(left: Value, right: Value) = object : Expression {
        override fun terms() = toTerms(left.value, left.zone).plus(toTerms(right.value, right.zone))

        override fun eval(hasWord: (Value) -> Boolean) = hasWord(left) && hasWord(right)
    }

    fun and(left: Value, right: Expression) = object : Expression {
        override fun terms() = toTerms(left.value, left.zone).plus(right.terms())

        override fun eval(hasWord: (Value) -> Boolean) = hasWord(left) && right.eval(hasWord)
    }

    fun and(left: Expression, right: Expression) = object : Expression {
        override fun terms() = left.terms().plus(right.terms())

        override fun eval(hasWord: (Value) -> Boolean) = left.eval(hasWord) && right.eval(hasWord)
    }
}

private fun toTerms(arg: String, zone: Zone) = kGram(arg).map { Value(it, zone) }.toSet()