package model

import java.io.File
import java.io.FileOutputStream
import java.util.concurrent.atomic.AtomicInteger

class VariableEncodingStorage(private val index: File, private val terms: File) : IndexStorage {

    constructor(dir: File) : this(File(dir, "index"), File(dir, "terms"))

    init {
        index.createNewFile()
        terms.createNewFile()

        require(index.isFile)
        require(terms.isFile)
    }

    private class StorageWriteOperationImp(index: File, terms: File) : IndexStorage.WriteOperation {
        private companion object {
            const val PAYLOAD_MASK = 0x7F
            const val STOP_BIT = 0x80
        }

        private val idGen = AtomicInteger(0)
        private val strToId = HashMap<String, Int>()
        private val ifos = FileOutputStream(index)
        private val tfos = FileOutputStream(terms)

        val ids = arrayListOf(824, 829, 215406)

        override fun onWrite(term: IndexStorage.Term) {
            writeTerm(term.token)

            class Holder {
                var docId: Int = 0
                val compressed: CompressedResult = CompressedResult()

                fun update(docId: Int): Holder {
                    this.docId = docId
                    return this
                }
            }

            var jj = 0

            val compressed = term.docs.fold(Holder()) { holder, doc ->
                val docId = strToId.getOrPut(doc) { idGen.incrementAndGet() }

                encode(docId - holder.docId, holder.compressed)
                holder.update(docId)
            }

            compressed.compressed.outData.forEach {
                println(it)
                ifos.write(it)
            }
        }

        override fun close() {
            ifos.close()
            tfos.close()
        }

        private data class CompressedResult(var lastByteIndx: Int = 0, val buffers: MutableList<Int> = mutableListOf(0)) {
            val outData
                get() = buffers.filter { it != 0 }
        }

        private fun encode(diff: Int, compressed: CompressedResult): CompressedResult {
            var _diff = diff
            var i = compressed.lastByteIndx
            var buffer = compressed.buffers[compressed.buffers.lastIndex]

            do {

                while (i < Integer.BYTES && _diff > 0) {
                    val lShift = Integer.SIZE - ((i + 1) shl 3)

                    buffer = buffer or ((_diff and PAYLOAD_MASK) shl lShift)
                    _diff = _diff shr 7 // only 7 bits for payload

                    if (_diff == 0) {
                        // we've done diff encoding, set stop bit
                        buffer = buffer or (STOP_BIT shl lShift)
                    }

                    ++i
                }

                compressed.buffers[compressed.buffers.lastIndex] = buffer
                // temp buffer is full, add it
                // to buffers collection and reset
                if (i == Integer.BYTES) {
                    i = 0
                    buffer = 0
                    compressed.buffers += buffer
                }
            } while (_diff > 0)

            compressed.lastByteIndx = i
            return compressed
        }

        private fun writeTerm(term: String) {
            val bArr = term.toByteArray()

            tfos.write(bArr.size)
            tfos.write(bArr)
        }

    }

    override fun writeOperation(): IndexStorage.WriteOperation = StorageWriteOperationImp(index, terms)

}