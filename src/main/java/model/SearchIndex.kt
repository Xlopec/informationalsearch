package model

interface SearchIndex {

    fun search(exp: Expression): Set<Book>

}

interface Expression {

    fun eval(hasWord: (Value) -> Boolean): Boolean

    fun terms(): Set<Value>

}

data class Value(val value: String, val zone: Zone = Zone.BODY)

object And {

    fun and(left: Value, right: Value) = object : Expression {
        override fun terms() = setOf(left, right)

        override fun eval(hasWord: (Value) -> Boolean) = hasWord(left) && hasWord(right)
    }

    fun and(left: Value, right: Expression) = object : Expression {
        override fun terms() = setOf(left).plus(right.terms())

        override fun eval(hasWord: (Value) -> Boolean) = hasWord(left) && right.eval(hasWord)
    }

    fun and(left: Expression, right: Expression) = object : Expression {
        override fun terms() = left.terms().plus(right.terms())

        override fun eval(hasWord: (Value) -> Boolean) = left.eval(hasWord) && right.eval(hasWord)
    }
}

object Or {

    fun or(left: Value, right: Value) = object : Expression {
        override fun terms() = setOf(left, right)

        override fun eval(hasWord: (Value) -> Boolean) = hasWord(left) || hasWord(right)
    }

    fun or(left: Value, right: Expression) = object : Expression {
        override fun terms() = setOf(left).plus(right.terms())

        override fun eval(hasWord: (Value) -> Boolean) = hasWord(left) || right.eval(hasWord)
    }

    fun or(left: Expression, right: Expression) = object : Expression {
        override fun terms() = left.terms().plus(right.terms())

        override fun eval(hasWord: (Value) -> Boolean) = left.eval(hasWord) || right.eval(hasWord)
    }
}

object Exact {
    fun of(value: Value) = object : Expression {
        private val terms = toTerms(value.value)

        override fun terms() = terms

        override fun eval(hasWord: (Value) -> Boolean) = terms.all { hasWord(it) }
    }
}

object Not {

    fun not(exp: Expression) = object : Expression {
        override fun terms() = exp.terms()

        override fun eval(hasWord: (Value) -> Boolean) = !exp.eval(hasWord)
    }
}

private fun toTerms(arg: String) = kGram(arg).map { Value(it) }.toSet()