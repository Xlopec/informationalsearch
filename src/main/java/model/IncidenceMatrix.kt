package model

class IncidenceMatrix(private val books: Array<out Book>) : SearchIndex {
    private val matrix: Array<Array<Boolean>>
    private val words: Array<String>

    init {
        words = books.map { book -> book.text.words }
                .fold(HashSet<String>(), { set, words -> set.addAll(words); set })
                .toTypedArray()

        matrix = Array(words.size, { i -> Array(books.size, { j -> books[j].text.words.contains(words[i]) }) })
    }

    override fun search(exp: Expression): Set<Book> {
        if (matrix.isEmpty()) {
            return emptySet()
        }

        val resultBooks = HashSet<Book>()
        var j = 0
        val findWord = { value: Value -> val i = words.indexOf(value.value); i < matrix.size && i >= 0 && matrix[i][j] }

        do {
            if (exp.eval(findWord)) {
                resultBooks += books[j]
            }
        } while (++j < books.size)

        return resultBooks
    }

}