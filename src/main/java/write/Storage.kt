package write

import model.Dictionary

interface Storage {

    fun store(dictionary: Dictionary)

}