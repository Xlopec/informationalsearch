package write

import model.Dictionary
import java.io.File
import java.io.FileOutputStream
import java.io.ObjectOutputStream

/**
 * Created by Максим on 1/21/2018.
 */
class SerializeStorage(private val file: File) : Storage {
    override fun store(dictionary: Dictionary) {
        ObjectOutputStream(FileOutputStream(file, false))
                .use { it.writeObject(dictionary.words); it.flush() }
    }
}