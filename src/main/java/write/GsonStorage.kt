package write
import com.google.gson.Gson
import model.Dictionary
import java.io.File

/**
 * Created by Максим on 1/21/2018.
 */
class GsonStorage(private val file: File) : Storage {

    override fun store(dictionary: Dictionary) {
        file.writeText(Gson().toJson(dictionary.words))
    }

}