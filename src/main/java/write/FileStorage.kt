package write

import model.Dictionary
import java.io.File

class FileStorage(private val file: File) : Storage {

    override fun store(dictionary: Dictionary) {
        file.writeText(dictionary.words.joinToString(","))
    }
}