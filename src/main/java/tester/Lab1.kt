package tester

import model.Book
import model.Dictionary
import read.PdfReader
import write.FileStorage
import write.GsonStorage
import write.SerializeStorage
import java.io.File
import java.io.FileInputStream

fun main(args: Array<String>) {
    val start = System.currentTimeMillis()
    val reader = PdfReader()
    val books : Set<Book> = args
            .map { path ->
                val file = File(path)
                reader.read(file.name, FileInputStream(file))
            }.toSet()

    val dictionary = Dictionary()

    books.forEach {
        dictionary.addText(it.text)
        println("Words in ${it.name}: ${it.text.words.size}")
    }

    FileStorage(File("result.txt")).store(dictionary)
    GsonStorage(File("result.json")).store(dictionary)
    SerializeStorage(File("result.ser")).store(dictionary)

    println("Total files count: ${args.size}")
    println("Total words: ${books.fold(0, { acc, book -> acc + book.wordsCount() })}")
    println("Total words in dictionary: ${dictionary.countWords()}")
    println("Spent time: ${System.currentTimeMillis() - start} msec")
}