package tester

import model.AndFuzzy
import model.Book
import model.InvertedIndexImp
import model.Value

fun main(args: Array<String>) {

    val books = arrayOf(Book("A", listOf("a", "b", "c")), Book("B", listOf("d", "e", "f")),
            Book("C", listOf("a", "Allah", "cc")))

    val index = InvertedIndexImp(books)

    println(index.search(AndFuzzy.and(Value("a"), Value("c"))))
}