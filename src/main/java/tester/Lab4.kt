package tester

import model.*

fun main(args: Array<String>) {

    println(shuffleIndex("man"))
    println(shuffleIndex("moron"))
    println(shuffleJoker("m*n"))
    println(kGram("month", 2))
    println(kGram("re*ve", 3))

    val books = arrayOf(Book("A", listOf("reive")), Book("B", listOf("removf", "c", "b")),
            Book("C", listOf("aa", "Allah", "cc")))

    val index = InvertedKGramIndex(books)

    println(index.search(Exact.of(Value("re*ve"))))
}