package tester

import SPLIT_PATTERN
import model.SPIMI
import model.VariableEncodingStorage
import java.io.File

fun main(args: Array<String>) {

    val files = collectFiles(File("Data"))
    val outDir = File("Merge")
    val start = System.currentTimeMillis()

    val algorithm = SPIMI(files.size / 10, true, storage = VariableEncodingStorage(outDir)) {
        it.readText().split(SPLIT_PATTERN)
    }

    val out = algorithm.createIndex(outDir, files)

    println("Spent ${formatInterval(System.currentTimeMillis() - start)} time to build index ($out)")
}

fun collectFiles(file: File): Set<File> {
    fun collect(file: File, set: MutableSet<File>): Set<File> {
        if (file.isFile) {
            set.add(file)
            return set
        }

        file.listFiles().forEach {
            if (it.isFile) {
                set.add(it)
            } else {
                set.addAll(collect(it, set))
            }
        }

        return set
    }

    return collect(file, HashSet())
}

fun formatInterval(ms: Long): String {
    val millis = ms % 1000
    var x = ms / 1000
    val seconds = x % 60
    x /= 60
    val minutes = x % 60
    x /= 60
    val hours = x % 24

    return String.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, millis)
}
