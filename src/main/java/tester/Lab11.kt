package tester

import SPLIT_PATTERN
import fb2.IBook
import fb2.IContainerNode
import fb2.INode
import fb2.parser.FB2Parser
import model.AndFuzzy
import model.Book
import model.InvertedIndexImp
import model.Value
import java.io.File

fun main(args: Array<String>) {

    val fb2s = args.map { File(it) }.map { toBook(FB2Parser.parse(it), it) }

    val index = InvertedIndexImp(fb2s)

    println(index.search(AndFuzzy.and(Value("HAPI"), Value("version"))).map { it.name })
}

private fun toBook(fb: IBook, file: File): Book {

    fun collect(node: INode, words: MutableCollection<String>): Collection<String> = when (node) {
        is IContainerNode -> {
            if (!node.title?.text.isNullOrBlank()) {
                words += node.text
            }

            node.childNodes.map { collect(it, words) }.flatten()
        }
        else -> {
            node.text?.let { node.text.split(SPLIT_PATTERN).filter { it.isNotBlank() } }?.also { words += it }

            words
        }
    }

    return Book(file.name, fb.bodies.map { collect(it, HashSet()) }.flatten())
}