package tester

import model.*
import query.DefaultQueryInterpreter
import query.QueryInterpreter
import read.PdfReader
import java.io.File
import java.io.FileInputStream

fun main(args: Array<String>) {

    val reader = PdfReader()
    val books = args
             .map { path ->
                 val file = File(path)
                 reader.read(file.name, FileInputStream(file))
             }
             .toSet()
             .toTypedArray()

    /*val books = arrayOf(Book("A", listOf("b")), Book("B", listOf("a", "c", "b")),
            Book("C", listOf("aa", "bb", "cc")))*/

    val found = InvertedIndex(books).search(And.and(Value("апрра"), Value("bb")))

    println(found)
    var input: String

    println("Type :q to quit")

    do {
        print("Which algorithm to use? (1 - inverted matrix, 2 - inverted index) >")
        input = readLine()!!

        when (input) {
            "1" -> runSearch(IncidenceMatrix(books))
            "2" -> runSearch(InvertedIndex(books))
            ":q" -> println("Quiting the program")
            else -> println("Invalid input")
        }
    } while (input.toLowerCase() != ":q")
}

private fun runSearch(search: SearchIndex, queryInterpreter: QueryInterpreter = DefaultQueryInterpreter()) {
    print("Search query >")
    println("Found books: ${search.search(queryInterpreter.interpret(readLine()!!)).map { it.name }}")
}
